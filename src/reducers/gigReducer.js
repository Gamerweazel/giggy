import * as types from '../actions/types';

const initialState = {
    gigs: [],
    fbGigs: [],
    googleGigs: [],
    error: ''
}

export default function gigReducer(state = initialState, action) {
    switch(action.type) {
        case types.INIT_GIGS:
            return {...state, gigs: action.gigs, fbGigs: action.fbGigs, googleGigs: action.googleGigs};
        case types.INIT_GOOGLE_GIGS:
            return {...state, googleGigs: action.payload};
        case types.INIT_FB_GIGS:
            return {...state, fbGigs: action.payload};
        case types.SAVE_GIG:
            return {...state, gigs: action.payload};
        case types.DELETE_GIG:
            return {...state, gigs: action.payload};
        case types.SYNC_FB_GIGS:
            return {...state, fbGigs: action.payload};
        case types.SYNC_GOOGLE_GIGS:
            return {...state, googleGigs: action.payload};
        case types.SYNC_ERROR:
            return {...state, error: action.payload};
        default: return state;
    }
}
