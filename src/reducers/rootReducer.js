import { combineReducers } from 'redux';
import authReducer from './authReducer';
import gigReducer from './gigReducer';

export default combineReducers({
    authReducer,
    gigReducer
});
