import * as types from '../actions/types';

const initState = {
    FB_AUTH: {
        STATUS: null,
        RES: null
    },
    FB_INIT: {
        STATUS: null
    },
    GOOGLE_AUTH: {
        STATUS: null,
        RES: null
    },
    GOOGLE_INIT: {
        STATUS: null
    }
}

export default function authReducer(state = initState, action) {
    switch (action.type) {
        case types.FB_INIT_LOADED:
            return {...state, FB_INIT: {STATUS: 'connected'}};
        case types.FB_INIT_FAILED:
            return {...state, FB_INIT: {STATUS: 'failed'}};
        case types.SUCCESS_LOGIN_FB:
            return {...state, FB_AUTH: {STATUS: 'connected', RES: action.payload}};
        case types.ERR_LOGIN_FB:
            return {...state, FB_AUTH: {STATUS: 'unauthorized', RES: null}};
        case types.FB_LOGOUT:
            return {...state, FB_AUTH: {STATUS: 'unauthorized', RES: null}};
        case types.GOOGLE_INIT_LOADED:
            return {...state, GOOGLE_INIT: {STATUS: 'connected'}};
        case types.GOOGLE_INIT_FAILED:
            return {...state, GOOGLE_INIT: {STATUS: 'failed'}};
        case types.SUCCESS_LOGIN_GOOGLE:
            return {...state, GOOGLE_AUTH: {STATUS: 'connected', RES: action.payload}};
        case types.ERR_LOGIN_GOOGLE:
            return {...state, GOOGLE_AUTH: {STATUS: 'unauthorized', RES: null}};
        default: return state;
    }
};
