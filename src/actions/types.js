// Auth
export const FB_INIT_LOADED = 'FB_INIT_LOADED';
export const FB_INIT_FAILED = 'FB_INIT_FAILED';
export const SUCCESS_LOGIN_FB = 'SUCCESS_LOGIN_FB';
export const FB_LOGIN = 'FB_LOGIN';
export const ERR_LOGIN_FB = 'ERR_LOGIN_FB';
export const FB_LOGOUT = 'FB_LOGOUT';
export const GOOGLE_INIT_LOADED = 'GOOGLE_INIT_LOADED';
export const GOOGLE_INIT_FAILED = 'GOOGLE_INIT_FAILED';
export const SUCCESS_LOGIN_GOOGLE = 'SUCCESS_LOGIN_GOOGLE';
export const ERR_LOGIN_GOOGLE = 'ERR_LOGIN_GOOGLE';

// Gigs
export const INIT_GIGS = 'INIT_GIGS';
export const CREATE_GIG = 'CREATE_GIG';
export const SAVE_GIG = 'SAVE_GIG';
export const DELETE_GIG = 'DELETE_GIG';
export const SYNC_FB_GIGS = 'SYNC_FB_GIGS';
export const SYNC_GOOGLE_GIGS = 'SYNC_GOOGLE_GIGS';
export const SYNC_ERROR = 'SYNC_ERROR';
