import * as types from './types';
import { syncFBGigs, syncGoogleGigs} from './gigActions';

export function fbLogout() {
    return dispatch => {
        window.FB.logout(res => {
            dispatch({
                type: types.FB_LOGOUT
            });
        });
    }
}

export function fbLogin() {
    return dispatch => {
        try {
            window.FB.login(res => {
                if (res && res.status === 'connected') {
                    dispatch({
                        type: types.SUCCESS_LOGIN_FB,
                        payload: res.authResponse
                    });
                    dispatch(syncFBGigs());
                } else {
                    dispatch({
                        type: types.ERR_LOGIN_FB
                    });
                }
            }, {scope: 'user_events'});
        } catch(e) {
            dispatch({
                type: types.ERR_LOGIN_FB
            });
        }
    }
}

export function googleLogin() {
    return dispatch => {
        try {
            window.gapi.auth.authorize({
                client_id: '765308486316-2944np2h811pp232vc18ddjm1cpcqdo7.apps.googleusercontent.com',
                scope: 'https://www.googleapis.com/auth/calendar.readonly',
                immediate: false
            }, res => {
                if (res && !res.error) {
                    dispatch({
                        type: types.SUCCESS_LOGIN_GOOGLE,
                        payload: res
                    });
                    dispatch(syncGoogleGigs());
                } else {
                    dispatch({
                        type: types.ERR_LOGIN_GOOGLE
                    });
                }
            });
        } catch(e) {
            dispatch({
                type: types.ERR_LOGIN_GOOGLE
            });
        }
    }
}

export function checkInit(item) {
    return dispatch => {
        let timeOut = 0,
            i = setInterval(checkLoadStatus, 1000),
            successType,
            successFunction,
            failType;

        if (item === 'fb') {
            successType = types.FB_INIT_LOADED;
            failType = types.FB_INIT_FAILED;
            successFunction = fbLogin();
        } else if (item === 'google') {
            successType = types.GOOGLE_INIT_LOADED;
            failType = types.GOOGLE_INIT_FAILED;
            successFunction = googleLogin();
        }

        function checkLoadStatus() {
            timeOut++;
            if (window[item]) {
                clearInterval(i);
                dispatch({
                   type: successType
               });
               if (JSON.parse(localStorage.getItem('syncOnEnter'))[item] === 'on') {
                   dispatch(successFunction);
               }
           }
           if (timeOut === 5 && !item) {
               clearInterval(i);
               dispatch({
                  type: failType
              });
           }
        }
    }
}
