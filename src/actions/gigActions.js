import * as types from './types';

export function initGigs() {
    return dispatch => {
        let gigs = localStorage.getItem('gigs') ?
        sortGigs(JSON.parse(localStorage.getItem('gigs'))) : [];
        let googleGigs = localStorage.getItem('googleGigs') ?
        sortGigs(JSON.parse(localStorage.getItem('googleGigs'))) : [];
        let fbGigs = localStorage.getItem('fbGigs') ?
        sortGigs(JSON.parse(localStorage.getItem('fbGigs'))) : [];
        if (gigs.length > 0) {
            for (let i = 0; i < gigs.length; i++) {
                gigs[i].daysUntil = getDaysLeft(gigs[i].date);
            }
        }
        if (googleGigs.length > 0) {
            for (let i = 0; i < googleGigs.length; i++) {
                googleGigs[i].daysUntil = getDaysLeft(googleGigs[i].date);
            }
        }
        if (fbGigs.length > 0) {
            for (let i = 0; i < fbGigs.length; i++) {
                fbGigs[i].daysUntil = getDaysLeft(fbGigs[i].date);
            }
        }
        dispatch({
            type: types.INIT_GIGS,
            gigs,
            googleGigs,
            fbGigs
        });
    }
}

export function saveGig(gig) {
    return (dispatch, getState) => {
        gig.daysUntil = getDaysLeft(gig.date);
        let gigs = getState().gigReducer.gigs,
            index = gigs.indexOf(gig);
        if (index > -1)
            gigs[index] = gig;
        else
            gigs.push(gig);
        sortGigs(gigs);
        localStorage.setItem('gigs', JSON.stringify(gigs));
        dispatch({
            type: types.SAVE_GIG,
            payload: gigs
        });
    };
};

export function deleteGig(gig) {
        return (dispatch, getState) => {
        let gigs = getState().gigReducer.gigs,
            index = gigs.indexOf(gig);
        if (index > -1)
            gigs.splice(index, 1);
        localStorage.setItem('gigs', JSON.stringify(gigs));
        dispatch({
            type: types.DELETE_GIG,
            payload: gigs
        });
    };
};

export function syncFBGigs() {
    return (dispatch, getState) => {
        window.FB.api('/me/events?type=attending', res => {
            let events = res.data,
                gigs = [...getState().gigReducer.fbGigs];
            if (events.length > 0) {
                for (let i = 0; i < events.length; i++) {
                    let event = events[i],
                        index = isAlreadyinGigs(event.id, gigs),
                        gig = constructGig(event.id,
                        event.name, event.start_time);
                    if (index > -1)
                        gigs[index] = gig;
                    else if (gig.daysUntil > -1)
                        gigs.push(gig);
                }
                sortGigs(gigs);
                localStorage.setItem('fbGigs', JSON.stringify(gigs));
                dispatch({
                    type: types.SYNC_FB_GIGS,
                    payload: gigs
                });
            } else {
                dispatch({
                    type: types.SYNC_ERROR,
                    payload: 'Nothing to show'
                });
            }
        });
    }
};

export function syncGoogleGigs() {
    return (dispatch, getState) => {
        try {
            window.gapi.client.load('calendar', 'v3', getEvents);

            function getEvents() {
                let request = window.gapi.client.calendar.events.list({
                 'calendarId': 'primary',
                 'timeMin': (new Date()).toISOString(),
                 'showDeleted': false,
                 'singleEvents': true,
                 'maxResults': 10,
                 'orderBy': 'startTime'
               });
                request.execute(res => {
                    let events = res.items,
                        gigs = [...getState().gigReducer.googleGigs];
                    if (events.length > 0) {
                        for (let i = 0; i < events.length; i++) {
                            let event = events[i],
                                index = isAlreadyinGigs(event.id, gigs),
                                gig = constructGig(event.id,
                                event.summary, event.start.dateTime)
                            if (index > -1)
                                gigs[index] = gig;
                            else if (gig.daysUntil > -1)
                                gigs.push(gig);
                        }
                        sortGigs(gigs);
                        localStorage.setItem('googleGigs', JSON.stringify(gigs));
                        dispatch({
                            type: types.SYNC_GOOGLE_GIGS,
                            payload: gigs
                        })
                    } else {
                        dispatch({
                            type: types.SYNC_ERROR,
                            payload: 'Nothing to show'
                        });
                    }
                });
            }
        } catch(e) {
            dispatch({
                type: types.SYNC_ERROR,
                payload: 'failed'
            });
        }
    }
}

function getDaysLeft(gigDate) {
    let day = 1000*60*60*24,
        evntDay = new Date(gigDate).getTime(),
        today = new Date().getTime();
    return 1 + Math.round((evntDay-today)/day)
}

function isAlreadyinGigs(checkId, list) {
    let index = -1;
    for (let i = 0; i < list.length; i++) {
        if (list[i].id === checkId)
            index = i;
    }
    return index;
}

function constructGig(id, title, date) {
    let gig = {id, title, date};
    gig.daysUntil = getDaysLeft(gig.date);
    return gig;
}

function sortGigs(list) {
    list.sort((a,b) => {
        if (a.daysUntil > b.daysUntil) {
            return 1;
        } else if (a.daysUntil < b.daysUntil) {
            return -1;
        } else {
            return 0;
        }
    });
    return list;
}
