import React, { Component } from 'react';
import logo from '../calendar.svg';
import '../css/menu.css';

export default class TopMenu extends Component {

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.makeVisible === this.props.makeVisible) return
        else if (nextProps.makeVisible) document.body.style.overflow = 'hidden';
        else document.body.style.overflow = '';
        return
    }

    render() {
        return(
            <div className={this.props.makeVisible ? 'menu' : 'menu notVisible'}>
                <div className='menuWrapper'>
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1>{this.props.heading}</h1>
                    <div>
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}
