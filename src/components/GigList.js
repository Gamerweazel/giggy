import React, { Component } from 'react';

export default class GigList extends Component {
    render() {
        const gigs = this.props.gigs.map(gig => {
                    return (
                        <div className={`gig ${this.props.styling}`} onClick={() => this.props.bindEdit(gig)} key={gig.id}>
                            <h4>{gig.title.substring(0,10)}...</h4>
                            <h2>{gig.daysUntil} {gig.daysUntil === 1 ? 'Day' : 'Days'}</h2>
                            <i style={{position: 'relative', top: '-35px', color: '#f1f1f1'}} className={`fa fa-${this.props.type}`}></i>
                        </div>
                    );
                });

        return (
            <div>
                {this.props.gigs.length > 0 ? gigs : null}
            </div>
            );
    }
}
