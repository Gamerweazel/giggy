import App from '../App';
import { connect } from 'react-redux';
import {
    fbLogin,
    fbLogout,
    googleLogin,
    checkInit
} from '../../actions/authActions';
import {
    initGigs,
    saveGig,
    deleteGig,
    syncFBGigs,
    syncGoogleGigs
} from '../../actions/gigActions';

const mapStateToProps = (state) => {
    return {
        fbAuth: state.authReducer.FB_AUTH,
        fbInit: state.authReducer.FB_INIT,
        googleAuth: state.authReducer.GOOGLE_AUTH,
        googleInit: state.authReducer.GOOGLE_INIT,
        gigs: state.gigReducer.gigs,
        googleGigs: state.gigReducer.googleGigs,
        fbGigs: state.gigReducer.fbGigs
    }
};

export default connect(mapStateToProps,
    {
        fbLogin,
        fbLogout,
        googleLogin,
        initGigs,
        saveGig,
        deleteGig,
        syncFBGigs,
        syncGoogleGigs,
        checkInit})(App);
