import React, { Component } from 'react';
import GigList from './GigList';
import TopMenu from './TopMenu'
import logo from '../calendar.svg';
import '../css/App.css';
import '../css/gig.css';
import '../css/snackBar.css';
import '../css/font-awesome.min.css';

export default class App extends Component {
    state = {
        editingGig: null,
        editingTitle: '',
        editingDate: '',
        showSnack: false,
        snackMessage: '',
        syncMenuOpen: false
    }

    componentWillMount() {
        if (!localStorage.getItem('syncOnEnter')) {
            localStorage.setItem('syncOnEnter', JSON.stringify({'fb': 'off',
            'google': 'off'}));
        }
        this.props.initGigs();
        this.props.checkInit('fb');
        this.props.checkInit('google');
        return
    }

    componentWillReceiveProps (nextProps) {
        if (nextProps.fbGigs !== this.props.fbGigs ||
            nextProps.googleGigs !== this.props.googleGigs) {
                this.replaceandShowMessage('Sync Complete!');
            }
    }

    setSyncMenu () {
        scroll(0,0);
        let state = this.state.syncMenuOpen ? false : true;
        this.setState({syncMenuOpen: state});
        if (state) document.body.addEventListener('click', () => {
            this.setState({syncMenuOpen: false});
            try {this.removeEventListener('click', arguments.callee).bind(this);}
            catch(e) {}
        });
    }

    checkLogStatusBeforeSync(desiredSync) {
        switch (desiredSync) {
            case 'fb':
                if (this.props.fbInit.STATUS === 'connected')
                    if (this.props.fbAuth.STATUS === 'connected')
                        this.props.syncFBGigs();
                    else
                        this.props.fbLogin();
                else
                    this.displayMessage('Unable to sync ;(');
                break;
            case 'google':
            if (this.props.googleInit.STATUS === 'connected')
                if (this.props.googleAuth.STATUS === 'connected')
                    this.props.syncGoogleGigs();
                else
                    this.props.googleLogin();
            else
                this.displayMessage('Unable to sync ;(');
                break;
            default: return;
        }
        this.setSyncMenu();
        return
    }

    changeSyncOnEnter(key) {
        let obj = JSON.parse(localStorage.getItem('syncOnEnter')),
            value = obj[key] === 'on' ? 'off' : 'on';
        obj[key] = value;
        localStorage.setItem('syncOnEnter', JSON.stringify(obj));
        return
    }

    displayMessage(message) {
        this.setState({
            showSnack: true,
            snackMessage: message
        })
        setTimeout(() => {
            this.setState({showSnack: false});
        }, 1000);
        return
    }

    replaceandShowMessage(message) {
        this.setState({
            editingGig: null,
            editingTitle: '',
            editingDate: '',
            syncMenuOpen: false
        });
        this.displayMessage(message);
        return
    }

    createGig() {
        scroll(0,0);
        this.setState({
            editingGig: {id: '_' + Math.random().toString(36).substr(2, 9)}
        });
        return
    }

    editGig = (gig) => {
        scroll(0,0);
        this.setState({
            editingGig: gig,
            editingTitle: gig.title,
            editingDate: gig.date
        });
        return
    }

    save() {
        if (this.state.editingTitle === '') {
            this.displayMessage('Enter a title');
            return;
        }
        if (this.state.editingDate === '') {
            this.displayMessage('Enter a date');
            return;
        } else {
            let gig = this.state.editingGig;
            gig.title = this.state.editingTitle;
            gig.date = this.state.editingDate;
            this.props.saveGig(gig);
            this.replaceandShowMessage('Saved!');
            return
        }
    }

    deleteGig(){
        this.props.deleteGig(this.state.editingGig);
        this.replaceandShowMessage('Deleted!');
        return
    }

    render(){
        const editingInterface = (
            <TopMenu heading='Enter Gig Details' makeVisible={this.state.editingGig}>
                <input type={'text'}
                    placeholder='Title'
                    value={this.state.editingTitle ? this.state.editingTitle : ''}
                    onChange={(e) => {
                        if (e.target.value.length > 30) return
                        else this.setState({editingTitle: e.target.value})
                    }
                    }/>
                <br />
                <input type={'date'}
                    placeholder="Date"
                    value={this.state.editingDate ? this.state.editingDate : ''}
                    onChange={(e) => this.setState({editingDate: e.target.value})}/>
                <br />
                <button className='primary'
                    onClick={() => this.deleteGig()}>Delete</button>
                <button className='secondary'
                    onClick={() => this.save()}>Save</button>
            </TopMenu>

        );

        const syncMenu = (
            <TopMenu heading='Sync' makeVisible={this.state.syncMenuOpen}>
                <div>
                    <button  className='google'
                        onClick={() => this.checkLogStatusBeforeSync('google')}>
                        <i className='fa fa-google'></i> Sync Google Calendar</button>
                </div>
                <input type='checkbox'
                    defaultChecked={JSON.parse(localStorage.getItem('syncOnEnter'))['google'] === 'on' ? true : false}
                    onChange={(e) => this.changeSyncOnEnter('google')} />
                <p style={{color: '#f1f1f1'}}>Sync on Enter</p>
                <div>
                    <button className='primary'
                        onClick={() => this.checkLogStatusBeforeSync('fb')}>
                        <i className='fa fa-facebook'></i> Sync Facebook Events</button>
                </div>
                <input type='checkbox'
                    defaultChecked={JSON.parse(localStorage.getItem('syncOnEnter'))['fb'] === 'on' ? true : false}
                    onChange={(e) => this.changeSyncOnEnter('fb')} />
                <p style={{color: '#f1f1f1'}}>Sync on Enter</p>
            </TopMenu>
        );

        const snackBar = (
            <div className={this.state.showSnack ? 'snackBar' : 'snackBar notVisible'}>{this.state.snackMessage}</div>
        );

        const syncButton = (
            <button style={{position: 'fixed', top: '5px',
            left: '0px', borderStyle: 'none'}} className='secondary'
                onClick={() => this.setSyncMenu()}>Sync</button>
        );
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h2>Giggit</h2>
                </div>
                <div>
                    <button className='secondary' style={{position: 'fixed',
                    bottom: '10px', right: '0px'}}
                        onClick={() => this.createGig()}>+</button>
                </div>
                <div className='gigContainer'>
                    <GigList gigs={this.props.gigs}
                        bindEdit={this.editGig} styling='userGig'
                        type='user'/>
                </div>
                <div className='gigContainer'>
                    <GigList gigs={this.props.googleGigs}
                        bindEdit={() => {return}} styling='googleGig'
                        type='google'/>
                </div>
                <div className='gigContainer'>
                    <GigList gigs={this.props.fbGigs}
                        bindEdit={() => {return}} styling='fbGig'
                        type='facebook'/>
                </div>
                {editingInterface}
                {syncMenu}
                {this.state.syncMenuOpen || this.state.editingGig ? null : syncButton}
                {this.props.gigs.length === 0 && this.props.googleGigs.length === 0
                    && this.props.fbGigs.length === 0 ? <h1>No gigs ;(</h1> : null}
                {snackBar}
            </div>
        );
    }
}
