import React from 'react';
import ReactDOM from 'react-dom';
import rootReducer from './reducers/rootReducer';
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router';
import { composeWithDevTools } from 'redux-devtools-extension';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import App from './components/redux/connectApp';
import './css/index.css';

const logger = createLogger();

const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(thunk, logger)
    )
);

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <App />
        </Provider>
    </BrowserRouter>,
  document.getElementById('root')
);
