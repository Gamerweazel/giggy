self.addEventListener('install', function(e) {
  e.waitUntil(
    caches.open('default').then(function(cache) {
    }));
});

self.addEventListener('install', function(e) {
  e.waitUntil(
    caches.open('default').then(function(cache) {
      return cache.addAll([
        '/',
        '/index.html',
        '/helpers/initScript.js',
        '/static/css/main.e021dcca.css',
        '/manifest.json',
        '/static/js/main.38a83f35.js',
        '/static/media/calendar.cc496482.svg',
        '/static/media/fontawesome-webfont.674f50d2.eot',
        '/static/media/fontawesome-webfont.912ec66d.svg',
        '/static/media/fontawesome-webfont.af7ae505.woff2',
        '/static/media/fontawesome-webfont.b06871f2.ttf',
        '/static/media/fontawesome-webfont.fee66e71.woff',
        '/favicon.ico',
        '/asset-manifest.json',
      ]);
  }).then(function() {
      return self.skipWaiting();
  }));
});

self.addEventListener('activate', function(e) {
    e.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
  );
});
