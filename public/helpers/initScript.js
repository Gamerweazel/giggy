(function(d) {
    var js, id = 'facebook-jssdk',
        ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

window.fbAsyncInit = function() {
    FB.init({
        appId: '1274441232628960',
        xfbml: true,
        cookie: true,
        version: 'v2.8'
    });
    window.fb = true;
}

function googleInit() {
    gapi.load('client', function() {
        window.google = true;
    });
}
